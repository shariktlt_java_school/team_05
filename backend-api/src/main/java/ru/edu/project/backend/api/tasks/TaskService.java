package ru.edu.project.backend.api.tasks;

import ru.edu.project.backend.api.common.AcceptorArgument;

import java.util.List;

public interface TaskService {

    /**
     * Получение списка доступных предметов.
     *
     * @return список
     */
    List<Subject> getAvailable();

    /**
     * Получение предметов по коду.
     *
     * @param ids
     * @return список
     */
    @AcceptorArgument
    List<Subject> getByIds(List<Long> ids);

    /**
     * Регистрация нового задания.
     *
     * @param taskForm
     * @return запись
     */
    @AcceptorArgument
    TaskInfo createTask(TaskForm taskForm);

    /**
     * Получение задания студента.
     *
     * @param id
     * @return список
     */
    TaskInfo getTaskByStudent(long id);

    /**
     * Получение списка всех заданий.
     * @return запись
     */
    List<TaskInfo> getAllTasks();

    /**
     * Запись оценок студенту.
     *
     * @param assessmentsList
     * @return TaskInfo
     */
    @AcceptorArgument
    TaskInfo makeAssessment(RequestToMakeAssessment assessmentsList);

}
