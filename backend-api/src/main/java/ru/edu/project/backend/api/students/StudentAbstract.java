package ru.edu.project.backend.api.students;

public interface StudentAbstract {

    /**
     * id.
     *
     * @return long
     */
    Long getId();

    /**
     * group_id.
     *
     * @return long
     */
    Long getGroupId();

    /**
     * record_id.
     *
     * @return long
     */
    Long getRecordId();

    /**
     * name.
     *
     * @return String
     */
    String getName();

    /**
     * second_name.
     *
     * @return String
     */
    String getSecondName();

    /**
     * last_name.
     *
     * @return String
     */
    String getLastName();

    /**
     * birthday_date.
     *
     * @return String
     */
    String getBirthdayDate();

    /**
     * phone.
     *
     * @return String
     */
    String getPhone();

    /**
     * entry_date.
     *
     * @return String
     */
    String getEntryDate();
}
