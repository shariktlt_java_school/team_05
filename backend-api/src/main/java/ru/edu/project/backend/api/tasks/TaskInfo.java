package ru.edu.project.backend.api.tasks;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.jackson.Jacksonized;
import ru.edu.project.backend.api.common.Status;

import java.sql.Timestamp;
import java.util.List;

@Getter
@Setter
@Builder
@Jacksonized
public class TaskInfo {

    /**
     * id (счетчик) задания.
     */
    private Long id;

    /**
     * id студента.
     */
    private Long studentId;

    /**
     * Статус.
     */
    private Status status;

    /**
     * Время создания задания.
     */
    private Timestamp createdAt;

    /**
     * Дата сдачи контрольных работ.
     */
    private Timestamp deadLine;

    /**
     * Комментарий преподавателя.
     */
    private String comment;

    /**
     * Выбранные предметы.
     */
    private List<Subject> subjects;

}
