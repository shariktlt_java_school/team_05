package ru.edu.project.backend.api.students;

import ru.edu.project.backend.api.common.AcceptorArgument;

import java.util.List;

public interface StudentService {

    /**
     * Добавление/редактирование данных студента.
     * @param studentForm
     * @return StudentInfo
     */
    @AcceptorArgument
    StudentInfo editStudent(StudentForm studentForm);

    /**
     * Просмотр информации о студенте.
     * @param recordId
     * @return StudentInfo
     */
    StudentInfo getDetailedInfo(Long recordId);

    /**
     * Вывод всех студентов.
     * @return List
     */
    List<StudentInfo> getAllStudents();

    /**
     * Вывод всех студентов по номеру группы.
     * @param groupId
     * @return List
     */
    List<StudentInfo> getStudentsByGroup(Long groupId);

    /**
     * Удаление студента.
     * @param recordId
     */
    void deleteStudent(Long recordId);
}

