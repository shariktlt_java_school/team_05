package ru.edu.project.backend.api.groups;

public interface GroupAbstract {
    /**
     * group_id.
     *
     * @return long
     */
    Long getGroupId();

    /**
     * name.
     *
     * @return string
     */
    String getName();

    /**
     * description.
     *
     * @return string
     */
    String getDescription();

    /**
     * students_count.
     *
     * @return long
     */
    Long getStudentsCount();
}
