package ru.edu.project.frontend.controller;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.ui.Model;
import ru.edu.project.authorization.UserDetailsId;
import ru.edu.project.backend.api.students.StudentInfo;
import ru.edu.project.backend.api.students.StudentService;
import ru.edu.project.backend.api.tasks.TaskInfo;
import ru.edu.project.backend.api.tasks.TaskService;

import java.util.ArrayList;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;

public class ReportControllerTest {

    @InjectMocks
        private ReportController reportController;

    @Mock
    private StudentService studentService;

    @Mock
    private TaskService taskService;

    @Mock
    private Model modelMock;

    @Mock
    private Authentication authenticationMock;

    @Mock
    private UserDetailsId userDetailsIdMock;

    @Before
    public void setUp() throws Exception {
        openMocks(this);
    }

    @Test
    public void indexTest() {

        long userId = 123L;

        when(authenticationMock.getPrincipal()).thenReturn(userDetailsIdMock);
        when(userDetailsIdMock.getUsername()).thenReturn("123");
        when(studentService.getDetailedInfo(userId)).thenReturn(null);
        when(taskService.getTaskByStudent(userId)).thenReturn(null);

        String expected = reportController.index(modelMock, authenticationMock);
        verify(modelMock).addAttribute("error", "");
        assertNull(modelMock.getAttribute("student"));
        assertNull(modelMock.getAttribute("task"));

        assertEquals(expected, "report/index");

        when(studentService.getDetailedInfo(userId)).thenReturn(StudentInfo.builder().build());
        when(taskService.getTaskByStudent(userId)).thenReturn(TaskInfo.builder().build());

        expected = reportController.index(modelMock, authenticationMock);

        assertEquals(expected, "report/index");
    }
}