package ru.edu.project.frontend.controller;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.ui.Model;
import org.springframework.web.servlet.ModelAndView;
import ru.edu.project.backend.api.tasks.RequestToMakeAssessment;
import ru.edu.project.backend.api.tasks.TaskForm;
import ru.edu.project.backend.api.tasks.TaskInfo;
import ru.edu.project.backend.api.tasks.TaskService;

import java.util.ArrayList;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;

public class TaskControllerTest {

    @InjectMocks
    private TaskController taskController;

    @Mock
    private TaskService taskService;

    @Mock
    private Model modelMock;

    @Mock
    private TaskController.CreateForm createFormMock;

    @Before
    public void setUp() throws Exception {
        openMocks(this);
    }

    @Test
    public void indexTest() {

        ArrayList<TaskInfo> info = new ArrayList<>();
        when(taskService.getAllTasks()).thenReturn(info);

        ModelAndView model = taskController.index();

        assertEquals("task/index", model.getViewName());
    }

    @Test
    public void viewTest() {

        long studentId = 123L;
        TaskInfo info = TaskInfo.builder().build();
        when(taskService.getTaskByStudent(studentId)).thenReturn(info);

        ModelAndView model = taskController.view(studentId);

        assertEquals("task/view", model.getViewName());

        assertEquals(info, model.getModel().get("record"));
    }

    @Test
    public void createFormTest() {

        when(taskService.getAvailable()).thenReturn(new ArrayList<>());

        String returnedString = taskController.createForm(modelMock);

        assertEquals(returnedString, "task/create");
    }

    @Test
    public void createFormProcessingTest() {

        TaskInfo expectedInfo = TaskInfo.builder().studentId(123L).build();

        TaskController.CreateForm createForm = new TaskController.CreateForm();
        createForm.setStudentId("12345");
        createForm.setDeadline("2022-03-09");
        createForm.setSubjects(new ArrayList<>());
        createForm.setComment("Hello");

        when(taskService.createTask(any(TaskForm.class))).thenAnswer(invocationOnMock -> {
            return expectedInfo;
        });

        String returnedString = taskController.createFormProcessing(createForm, modelMock);

        assertEquals(returnedString, "redirect:/task/?created=" + expectedInfo.getStudentId());
    }

    @Test
    public void assessmentFormTest() {

        long studentId = 123L;

        TaskInfo expectedInfo = TaskInfo.builder().build();
        when(taskService.getTaskByStudent(studentId)).thenReturn(expectedInfo);

        ModelAndView modelAndView = taskController.assessmentForm(studentId);

        assertEquals(expectedInfo, modelAndView.getModel().get("subjects"));
    }

    @Test
    public void assessmentFormProcessing() {

        Long studentId = 123L;

        TaskController.AssessmentForm assessmentForm = new TaskController.AssessmentForm();
        assessmentForm.setAssess1("1");
        assessmentForm.setAssess2("2");
        assessmentForm.setAssess3("3");
        assessmentForm.setAssess4("4");

        String expected = taskController.assessmentFormProcessing(assessmentForm, studentId);

        assertEquals(expected, "redirect:/task/?updated=" + studentId);

    }
}