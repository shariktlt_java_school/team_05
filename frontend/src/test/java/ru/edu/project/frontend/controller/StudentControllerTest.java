package ru.edu.project.frontend.controller;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.ui.Model;
import org.springframework.web.servlet.ModelAndView;
import ru.edu.project.backend.api.groups.GroupForm;
import ru.edu.project.backend.api.groups.GroupInfo;
import ru.edu.project.backend.api.groups.GroupService;
import ru.edu.project.backend.api.students.StudentForm;
import ru.edu.project.backend.api.students.StudentInfo;
import ru.edu.project.backend.api.students.StudentService;
import ru.edu.project.backend.api.tasks.TaskInfo;
import ru.edu.project.backend.api.tasks.TaskService;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.openMocks;

public class StudentControllerTest {

    @Mock
    private GroupService groupService;

    @Mock
    private StudentService studentService;

    @Mock
    private TaskService taskService;

    @InjectMocks
    private StudentController studentController;

    @Before
    public void setUp() throws Exception {
        openMocks(this);
    }

    @Test
    public void index() {
        Model model = mock(Model.class);
        List<StudentInfo> studentInfoList = new ArrayList<>();
        when(studentService.getAllStudents()).thenReturn(studentInfoList);

        String view = studentController.index(model);
        assertEquals("student/index", view);
        verify(model).addAttribute("students", studentInfoList);
    }

    @Test
    public void view() {
        Long recordId = 123L;
        StudentInfo studentInfo = StudentInfo.builder().build();
        TaskInfo taskInfo = TaskInfo.builder().build();

        when(studentService.getDetailedInfo(recordId)).thenReturn(null);

        ModelAndView model = studentController.view(recordId);

        assertEquals("student/idNotFound", model.getViewName());
        assertTrue(model.getStatus().is4xxClientError());


        when(studentService.getDetailedInfo(recordId)).thenReturn(studentInfo);
        when(taskService.getTaskByStudent(recordId)).thenReturn(null);

        model = studentController.view(recordId);

        assertEquals("student/view", model.getViewName());
        assertEquals(studentInfo, model.getModel().get("student"));
        assertNull(model.getModel().get("task"));

        when(taskService.getTaskByStudent(recordId)).thenReturn(taskInfo);

        model = studentController.view(recordId);

        assertEquals("student/view", model.getViewName());
        assertEquals(studentInfo, model.getModel().get("student"));
        assertEquals(taskInfo, model.getModel().get("task"));

    }

    @Test
    public void editFormWithId() {
        Long recordId = 123L;
        StudentInfo studentInfo = StudentInfo.builder().build();
        List<GroupInfo> groupInfoList = new ArrayList<>();

        ModelAndView model = studentController.editForm((Long) null);
        assertEquals("student/edit", model.getViewName());
        assertNull(model.getModel().get("student"));
        assertNull(model.getModel().get("groups"));

        when(studentService.getDetailedInfo(recordId)).thenReturn(null);
        model = studentController.editForm(recordId);

        assertEquals("student/idNotFound", model.getViewName());
        assertTrue(model.getStatus().is4xxClientError());

        when(studentService.getDetailedInfo(recordId)).thenReturn(studentInfo);
        when(groupService.getAllGroups()).thenReturn(groupInfoList);
        model = studentController.editForm(recordId);

        assertEquals("student/edit", model.getViewName());
        assertEquals(studentInfo, model.getModel().get("student"));
        assertEquals(groupInfoList, model.getModel().get("groups"));
    }

    @Test
    public void editForm() {
        Model model = mock(Model.class);
        List<GroupInfo> groupInfoList = new ArrayList<>();

        when(groupService.getAllGroups()).thenReturn(groupInfoList);

        String view = studentController.editForm(model);
        assertEquals("student/noGroups", view);
        verify(model, never()).addAttribute("groups", groupInfoList);

        groupInfoList.add(GroupInfo.builder().build());

        view = studentController.editForm(model);
        assertEquals("student/edit", view);
        verify(model).addAttribute("groups", groupInfoList);
    }

    @Test
    public void deleteForm() {
        Long recordId = 123L;
        StudentInfo studentInfo = StudentInfo.builder().build();
        List<StudentInfo> studentInfoList = new ArrayList<>();

        when(studentService.getDetailedInfo(recordId)).thenReturn(null);

        ModelAndView model = studentController.deleteForm(recordId);
        assertEquals("student/idNotFound", model.getViewName());
        assertTrue(model.getStatus().is4xxClientError());

        when(studentService.getDetailedInfo(recordId)).thenReturn(studentInfo);
        when(studentService.getAllStudents()).thenReturn(studentInfoList);

        model = studentController.deleteForm(recordId);
        verify(studentService).deleteStudent(recordId);
        assertEquals(recordId, model.getModel().get("deleted"));
        assertEquals(studentInfoList, model.getModel().get("students"));
        assertEquals("student/index", model.getViewName());
    }

    @Test
    public void editFormProcessing() {
        Long recordId = 123L;
        StudentController.CreateForm createForm = new StudentController.CreateForm();
        createForm.setName("name");
        createForm.setGroupId(12L);
        createForm.setLastName("last_name");
        createForm.setSecondName("second_name");
        createForm.setBirthdayDate("2022-01-01");
        createForm.setPhone("phone");

        StudentInfo studentInfo = StudentInfo.builder().build();

        when(studentService.editStudent(any(StudentForm.class))).thenAnswer(invocationOnMock -> {
            StudentForm form = invocationOnMock.getArgument(0, StudentForm.class);
            assertEquals(createForm.getName(), form.getName());
            assertEquals(createForm.getBirthdayDate(), form.getBirthdayDate());
            assertEquals(createForm.getGroupId(), form.getGroupId());
            assertEquals(createForm.getLastName(), form.getLastName());
            assertEquals(createForm.getSecondName(), form.getSecondName());
            return studentInfo;
        });

        String viewName = studentController.editFormProcessing(createForm, recordId);
        assertEquals("redirect:/student/?edited=" + studentInfo.getGroupId(), viewName);
        verify(studentService, times(1)).editStudent(any(StudentForm.class));

        viewName = studentController.editFormProcessing(createForm, null);
        assertEquals("redirect:/student/?edited=" + studentInfo.getGroupId(), viewName);
        verify(studentService, times(2)).editStudent(any(StudentForm.class));

    }
}