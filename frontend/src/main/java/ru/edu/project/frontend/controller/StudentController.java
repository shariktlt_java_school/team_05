package ru.edu.project.frontend.controller;


import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.edu.project.backend.api.groups.GroupService;
import ru.edu.project.backend.api.students.StudentForm;
import ru.edu.project.backend.api.students.StudentInfo;
import ru.edu.project.backend.api.students.StudentService;
import ru.edu.project.backend.api.tasks.TaskInfo;
import ru.edu.project.backend.api.tasks.TaskService;


import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import java.time.LocalDate;


@RequestMapping("/student")
@Controller
public class StudentController {

    /**
     * Ссылка на сервис студентов.
     */
    @Autowired
    private StudentService studentService;

    /**
     * Ссылка на сервис групп.
     */
    @Autowired
    private GroupService groupService;

    /**
     * Ссылка на сервис заданий.
     */
    @Autowired
    private TaskService taskService;

    /**
     * Отображение списка студентов.
     *
     * @param model
     * @return path
     */
    @GetMapping("/")
    public String index(final Model model) {
        model.addAttribute("students", studentService.getAllStudents());
        return "student/index";
    }

    /**
     * Отображение информации по студенту по номеру зачетки.
     * @param recordId
     * @return ModelAndView
     */
    @GetMapping("/view/{id}")
    public ModelAndView view(final @PathVariable("id") Long recordId) {
        ModelAndView model = new ModelAndView("student/view");

        StudentInfo studentInfo = studentService.getDetailedInfo(recordId);
        if (studentInfo == null) {
            model.setStatus(HttpStatus.NOT_FOUND);
            model.setViewName("student/idNotFound");
            return model;
        }

        model.addObject("student", studentInfo);

        TaskInfo taskInfo = taskService.getTaskByStudent(recordId);
        if (taskInfo != null) {
            model.addObject("task", taskInfo);
        }
        return model;
    }

    /**
     * Отображение формы студента.
     * @param recordId
     * @return ModelAndView
     */
    @GetMapping("/edit/{id}")
    public ModelAndView editForm(final @PathVariable("id") Long recordId) {
        ModelAndView model = new ModelAndView("student/edit");

        if (recordId != null) {
            StudentInfo studentInfo = studentService.getDetailedInfo(recordId);
            if (studentInfo == null) {
                model.setStatus(HttpStatus.NOT_FOUND);
                model.setViewName("student/idNotFound");
                return model;
            }
            model.addObject("student", studentInfo);
            model.addObject("groups", groupService.getAllGroups());
        }
        return model;
    }

    /**
     * Отображение формы студента.
     * @param model
     * @return path
     */
    @GetMapping("/edit")
    public String editForm(final Model model) {
        if (!groupService.getAllGroups().isEmpty()) {
            model.addAttribute("groups", groupService.getAllGroups());
            return "student/edit";
        }
        return "student/noGroups";

    }

    /**
     * Удаление данных студента.
     * @param recordId
     * @return ModelAndView
     */
    @GetMapping("/delete/{id}")
    public ModelAndView deleteForm(final @PathVariable("id") Long recordId) {
        ModelAndView model = new ModelAndView("student/index");
        StudentInfo studentInfo = studentService.getDetailedInfo(recordId);
        if (studentInfo == null) {
            model.setStatus(HttpStatus.NOT_FOUND);
            model.setViewName("student/idNotFound");
            return model;
        }
        studentService.deleteStudent(recordId);
        model.addObject("deleted", recordId);
        model.addObject("students", studentService.getAllStudents());
        return model;
    }

    /**
     * Создание/редактирование студента.
     * @param form
     * @param recordId
     * @return path
     */
    @PostMapping(value = {"/edit/{recordId}", "/edit"})
    public String editFormProcessing(
            @Valid
            @ModelAttribute final CreateForm form,
            final @PathVariable(required = false) Long recordId
    ) {
        StudentInfo studentInfo = studentService.editStudent(StudentForm.builder()
                .name(form.getName())
                .lastName(form.getLastName())
                .secondName(form.getSecondName())
                .phone(form.getPhone())
                .groupId(form.getGroupId())
                .birthdayDate(form.getBirthdayDate())
                .recordId(recordId)
                .build());

        return "redirect:/student/?edited=" + studentInfo.getRecordId();
    }

    @Getter
    @Setter
    public static class CreateForm {
        /**
         * Имя студента.
         */
        @NotNull
        private String name;

        /**
         * Отчество.
         */
        private String secondName;

        /**
         * Фамилия.
         */
        @NotNull
        private String lastName;

        /**
         * Номер телефона.
         */
        @NotNull
        private String phone;

        /**
         * Группа.
         */
        @NotNull
        private Long groupId;

        /**
         * Дата рождения студента.
         */
        @NotNull
        private String birthdayDate;

        /**
         * Получение объекта даты.
         *
         * @return календарь
         */
        @SneakyThrows
        public LocalDate getBirthdayDate() {
            return LocalDate.parse(birthdayDate);
        }

    }

}
