package ru.edu.project.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan({"ru.edu.project"})
public class FrontendApplication {

    /**
     * Точка входа.
     *
     * @param args input parameters
     */
    public static void main(final String[] args) {
        SpringApplication.run(FrontendApplication.class, args);
    }

}
