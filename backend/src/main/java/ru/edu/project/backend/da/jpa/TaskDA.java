package ru.edu.project.backend.da.jpa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.edu.project.backend.api.tasks.RequestToMakeAssessment;
import ru.edu.project.backend.api.tasks.Subject;
import ru.edu.project.backend.api.tasks.TaskForm;
import ru.edu.project.backend.api.tasks.TaskInfo;
import ru.edu.project.backend.da.jpa.entity.SubjectEntity;
import ru.edu.project.backend.da.jpa.entity.TaskEntity;
import ru.edu.project.backend.da.jpa.repository.SubjectEntityRepository;
import ru.edu.project.backend.da.jpa.repository.TaskEntityRepository;
import ru.edu.project.backend.model.Task;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

@Service
public class TaskDA {

    /**
     * Константа. Смещение в id для подзаданий.
     */
    public static final int SHIFT = 10000;

    /**
     * Локальный счетчик заданий.
     */
    private AtomicLong idCount = new AtomicLong(0);

    /**
     * Зависимость.
     */
    @Autowired
    private TaskEntityRepository taskEntityRepository;

    /**
     * Зависимость.
     */
    @Autowired
    private SubjectEntityRepository subjectEntityRepository;

    /**
     * Для отладки.
     * Получаем дерево задач
     *
     * @param studentId номер студента
     * @return дерево
     */
    public List<Task> getByStudent(final long studentId) {
        Map<Long, Task> entityMap = new HashMap<>();
        List<TaskEntity> allByStudentId = taskEntityRepository.findAllByStudentId(studentId);

        allByStudentId.forEach(task -> {
            if (task.getParentId() != null) {

                // Если это подзадача (учебный предмет)

                if (!entityMap.containsKey(task.getParentId())) {

                    // На случай, если пока основное здание не записано в мапу
                    // и невозможно добавить к нему предмет.
                    // Создается основное задание с пустым списком подзадач (предметов)
                    // не проверено
                    entityMap.put(task.getParentId(), Task.builder()
                            .subTasks(new ArrayList<>())
                            .build());
                }

                // Добавление предметов в список основного задания
                entityMap.get(task.getParentId()).getSubTasks().add(mapTaskEntityToTask(task));

            } else {

                // Если это основная задача

                if (!entityMap.containsKey(task.getId())) {

                    // Записываем в мапу новое задание
                    entityMap.put(task.getId(), mapTaskEntityToTask(task));
                } else {

                    // Обновляем задание
                    Task saved = entityMap.get(task.getId());
                    saved.setStudentId(studentId);
                    saved.setAssessment(task.getAssessment());
                    saved.setTaskSubtask(task.getTaskSubtask());
                }
            }
        });


        return new ArrayList<>(entityMap.values());
    }

    /**
     * Для отладки.
     * Сохраняем
     *
     * @param task
     * @return task
     */
    public Task save(final Task task) {

        TaskEntity draft = new TaskEntity();
        draft.setId(task.getId());
        draft.setStudentId(task.getStudentId());
        draft.setStatus(task.getStatus());
        draft.setCreatedAt(task.getCreatedAt());
        draft.setDeadLine(task.getDeadLine());
        draft.setComment(task.getComment());
        draft.setParentId(task.getParentId());
        draft.setTaskSubtask(task.getTaskSubtask());
        draft.setAssessment(task.getAssessment());

        return mapTaskEntityToTask(taskEntityRepository.save(draft));
    }

    private Task mapTaskEntityToTask(final TaskEntity task) {
        return Task.builder()
                .id(task.getId())
                .studentId(task.getStudentId())
                .status(task.getStatus())
                .createdAt(task.getCreatedAt())
                .deadLine(task.getDeadLine())
                .comment(task.getComment())
                .parentId(task.getParentId())
                .taskSubtask(task.getTaskSubtask())
                .assessment(task.getAssessment())
                .subTasks(new ArrayList<>())
                .build();
    }

    /**
     * Получение всех заданий.
     *
     * @return список заданий
     */
    public List<TaskInfo> getAllTasks() {

        List<TaskEntity> allFromDataBase = taskEntityRepository.findAll();

        List<TaskInfo> subTasks = new ArrayList<>();

        allFromDataBase.forEach(task -> {
            if (task.getParentId() == null) {

                // Если это основная задача

                // Добавление задачи в список для отображения
                subTasks.add(mapMainTaskInfo(task));

            }
        });

        if (subTasks.size() == 0) {
            return Collections.emptyList();
        }

        return subTasks;
    }

    private TaskInfo mapMainTaskInfo(final TaskEntity task) {

        TaskInfo newTaskInfo = TaskInfo.builder()
                .id(task.getId())
                .studentId(task.getStudentId())
                .status(null)
                .createdAt(task.getCreatedAt())
                .deadLine(task.getDeadLine())
                .comment(task.getComment())
                .subjects(new ArrayList<>())
                .build();

        return newTaskInfo;
    }

    /**
     * Регистрация нового задания.
     *
     * @param taskForm
     * @return запись
     */
    public TaskInfo createTask(final TaskForm taskForm) {

        TaskInfo info = TaskInfo.builder()
                .id(taskForm.getStudentId())
                .studentId(taskForm.getStudentId())
                .createdAt(new Timestamp(new Date().getTime()))
                .deadLine(taskForm.getDeadLine())
                .status(null)
                .comment(taskForm.getComment())
                .subjects(getSubjectsById(taskForm))
                .build();

        TaskEntity taskEntity = new TaskEntity();

        if (info.getId() > 0 || info.getId() != null) {

            // Сохранение родительского задания

            taskEntity.setId(info.getId());
            taskEntity.setParentId(null);
            taskEntity.setTaskSubtask(
                    "Основное задание №" + info.getId() + " студента №" + info.getStudentId());

            partialFillingInTaskEntity(taskEntity, info);

            taskEntityRepository.save(taskEntity);

            if ((info.getSubjects().size() > 0)) {

                // Сохраняем подзадание - заданный предмет
                int i = 1;
                for (Subject subject : info.getSubjects()) {

                    TaskEntity draft = new TaskEntity();

                    draft.setId(info.getId() * SHIFT + i++);
                    draft.setParentId(info.getId());
                    draft.setTaskSubtask(subject.getTitle());

                    partialFillingInTaskEntity(draft, info);

                    taskEntityRepository.save(draft);
                }
            }
        }

        return info;
    }

    private void partialFillingInTaskEntity(final TaskEntity taskEntity, final TaskInfo info) {
        taskEntity.setStudentId(info.getStudentId());
        taskEntity.setStatus(null);
        taskEntity.setCreatedAt(info.getCreatedAt());
        taskEntity.setDeadLine(info.getDeadLine());
        taskEntity.setComment(info.getComment());
        taskEntity.setAssessment(null);
    }

    private List<Subject> getSubjectsById(final TaskForm taskForm) {

        List<SubjectEntity> subjectListFromDB = subjectEntityRepository.findAll();

        List<Subject> subjectList = new ArrayList<>();

        List<Long> subjectIndexes = taskForm.getSelectedSubjects();

        for (Long index : subjectIndexes) {

            int i = Integer.parseInt(index.toString()) - 1;

            Subject subject = new Subject();

            subject.setId(subjectListFromDB.get(i).getId());
            subject.setTitle(subjectListFromDB.get(i).getTitle());
            subject.setDesc(subjectListFromDB.get(i).getDesc());

            subjectList.add(subject);
        }

        return subjectList;
    }

    /**
     * Просмотр задания по id.
     *
     * @param studentId номер студента
     * @return информация по заданиам студента
     */
    public TaskInfo getTaskByStudent(final Long studentId) {

        List<TaskEntity> allByStudentId = taskEntityRepository.findAllByStudentId(studentId);

        TaskInfo info = createEmptyTaskInfo();

        List<Subject> subjectList = new ArrayList<>();

        allByStudentId.forEach(task -> {
            if (task.getParentId() == null) {
                // Если это основная задача

                info.setId(task.getId());
                        info.setStudentId(task.getStudentId());
                        info.setStatus(null);
                        info.setCreatedAt(task.getCreatedAt());
                        info.setDeadLine(task.getDeadLine());
                        info.setComment(task.getComment());
                        info.setSubjects(new ArrayList<Subject>());

            } else {
                // Если это подзадача (учебный предмет)

                Subject subject = new Subject();

                subject.setId(task.getId());
                subject.setTitle(task.getTaskSubtask());
                subject.setDesc(task.getComment());
                subject.setAssessment(task.getAssessment());

                subjectList.add(subject);
            }
        });

        info.setSubjects(subjectList);

        return info;

    }

    private TaskInfo createEmptyTaskInfo() {

        TaskInfo info = TaskInfo.builder()
                .id(null)
                .studentId(null)
                .status(null)
                .createdAt(null)
                .deadLine(null)
                .comment("empty")
                .subjects(new ArrayList<Subject>())
                .build();

        return info;

    }

    /**
     * Проставление оценок студенту.
     *
     * @param assessmentsList коды предметов
     * @return TaskInfo
     */
    public TaskInfo makeAssessment(final RequestToMakeAssessment assessmentsList) {

        Long studentId = assessmentsList.getStudentId();

        List<TaskEntity> allByStudentId = taskEntityRepository.findAllByStudentId(studentId);

        int i = 0;

        for (TaskEntity taskEntity: allByStudentId) {

            if (taskEntity.getParentId() != null) {
                // Найдено подзадание (предмет)
                taskEntity.setAssessment(assessmentsList.getAssessments()[i++]);

                taskEntityRepository.save(taskEntity);
            }
        }

        return createEmptyTaskInfo();
    }
}
