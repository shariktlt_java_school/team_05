package ru.edu.project.backend.da.jdbctemplate;

import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Service;
import ru.edu.project.backend.api.students.StudentInfo;
import ru.edu.project.backend.da.StudentDALayer;
import ru.edu.project.backend.service.StudentServiceLayer;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Profile("JDBC_TEMPLATE")
public class StudentDA implements StudentDALayer {

    /**
     * Запрос для вывода всех записей.
     */
    public static final String QUERY_ALL_STUDENTS = "SELECT * FROM student";

    /**
     * Запрос для поиска студента по номеру зачетки.
     */
    public static final String QUERY_FOR_STUDENT_ID = "SELECT * FROM student WHERE record_id = ?";

    /**
     * Запрос для поиска студентов по номеру группы.
     */
    public static final String QUERY_FOR_STUDENT_BY_GROUP_ID = "SELECT * FROM student WHERE group_id = ?";

    /**
     * Обновление данных студента по номеру зачетки.
     * Обновляем только изменяемые поля.
     */
    public static final String QUERY_FOR_UPDATE_BY_RECORD_ID = "UPDATE student SET group_id = :group_id, name = :name, second_name = :second_name, last_name = :last_name, birthday_date = :birthday_date, phone = :phone, entry_date = :entry_date WHERE record_id = :record_id";

    /**
     * Обновление данных студента по номеру зачетки.
     * Обновляем только изменяемые поля.
     */
    public static final String DELETE_BY_RECORD_ID = "DELETE FROM student WHERE record_id = ?";

    /**
     * Зависимость на шаблон jdbc.
     */
    @Autowired
    private JdbcTemplate jdbcTemplate;

    /**
     * Зависимость на шаблон jdbc insert.
     */
    private SimpleJdbcInsert jdbcInsert;

    /**
     * Зависимость на шаблон jdbc named.
     */
    @Autowired
    private NamedParameterJdbcTemplate jdbcNamed;

    /**
     * Внедрение зависимости jdbc insert с настройкой для таблицы.
     *
     * @param bean
     */
    @Autowired
    public void setJdbcInsert(final SimpleJdbcInsert bean) {
        jdbcInsert = bean
                .withTableName("student")
                .usingGeneratedKeyColumns("id");
    }

    /**
     * Получение данных студента по номеру зачетки.
     *
     * @param recordId
     * @return student
     */
    @Override
    public StudentInfo getById(final Long recordId) {
        return jdbcTemplate.query(QUERY_FOR_STUDENT_ID, this::singleRowMapper, recordId);
    }

    /**
     * Сохранение (создание/обновление) данных студента.
     *
     * @param draft
     * @return student
     */
    @Override
    public StudentInfo save(final StudentInfo draft) {
        if (draft.getRecordId() != null) {
            return update(draft);
        } else {
            return insert(draft);
        }
    }

    /**
     * Получение списка студентов.
     *
     * @return list students
     */
    @Override
    public List<StudentInfo> getAll() {
        return jdbcTemplate.query(QUERY_ALL_STUDENTS, this::rowMapper);
    }

    /**
     * Получение списка студентов по номеру группы.
     *
     * @param groupId
     * @return list students
     */
    @Override
    public List<StudentInfo> getStudentsByGroup(final Long groupId) {
        return jdbcTemplate.query(QUERY_FOR_STUDENT_BY_GROUP_ID, this::rowMapper, groupId);
    }

    /**
     * Удаление данных студента.
     *
     * @param recordId
     */
    @Override
    public void deleteStudentById(final Long recordId) {
        jdbcTemplate.query(DELETE_BY_RECORD_ID, this::singleRowMapper, recordId);
    }

    private StudentInfo update(final StudentInfo draft) {
        int i = jdbcNamed.update(QUERY_FOR_UPDATE_BY_RECORD_ID, toMap(draft));
        return draft;
    }


    private StudentInfo insert(final StudentInfo draft) {
        long idInGroup = getStudentsByGroup(draft.getGroupId()).size() + 1;
        draft.setRecordId(StudentServiceLayer.createRecordId(draft.getGroupId(), idInGroup));

        jdbcInsert.executeAndReturnKey(toMap(draft)).longValue();
        return draft;
    }

    private Map<String, Object> toMap(final StudentInfo draft) {
        HashMap<String, Object> map = new HashMap<>();
        map.put("record_id", draft.getRecordId());
        map.put("group_id", draft.getGroupId());
        map.put("name", draft.getName());
        map.put("second_name", draft.getSecondName());
        map.put("last_name", draft.getLastName());
        map.put("birthday_date", draft.getBirthdayDate());
        map.put("phone", draft.getPhone());
        map.put("entry_date", draft.getEntryDate());

        return map;
    }


    @SneakyThrows
    private StudentInfo rowMapper(final ResultSet rs, final int pos) {
        return mapRow(rs);
    }

    @SneakyThrows
    private StudentInfo singleRowMapper(final ResultSet rs) {
        rs.next();
        return mapRow(rs);
    }

    private StudentInfo mapRow(final ResultSet rs) throws SQLException {
        return StudentInfo.builder()
                .groupId(rs.getLong("group_id"))
                .recordId(rs.getLong("record_id"))
                .name(rs.getString("name"))
                .secondName(rs.getString("second_name"))
                .lastName(rs.getString("last_name"))
                .birthdayDate(LocalDate.parse(rs.getString("birthday_date")))
                .phone(rs.getString("phone"))
                .entryDate(LocalDate.parse(rs.getString("entry_date")))
                .build();
    }
}
