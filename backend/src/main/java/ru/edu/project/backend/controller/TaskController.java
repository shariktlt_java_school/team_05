package ru.edu.project.backend.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.edu.project.backend.api.tasks.RequestToMakeAssessment;
import ru.edu.project.backend.api.tasks.Subject;
import ru.edu.project.backend.api.tasks.TaskForm;
import ru.edu.project.backend.api.tasks.TaskInfo;
import ru.edu.project.backend.api.tasks.TaskService;
import ru.edu.project.backend.da.jpa.SubjectDA;
import ru.edu.project.backend.da.jpa.TaskDA;
import ru.edu.project.backend.model.Task;

import java.util.List;

@RestController
@RequestMapping("/task")
public class TaskController implements TaskService {

    /**
     * Зависимость.
     * Таблица task
     */
    @Autowired
    private TaskDA taskDA;

    /**
     * Зависимость.
     * Таблица subject_type
     */
    @Autowired
    private SubjectDA subjectDA;

    /**
     * Получение списка доступных предметов.
     *
     * @return список
     */
    @Override
    @GetMapping("/getAvailable")
    public List<Subject> getAvailable() {

        return subjectDA.getAvailable();
    }

    /**
     * Получение предметов по коду.
     *
     * @param ids
     * @return список
     */
    @Override
    public List<Subject> getByIds(final List<Long> ids) {
        return null;
    }

    /**
     * Регистрация нового задания.
     *
     * @param taskForm
     * @return запись
     */
    @Override
    @PostMapping("/createTask")
    public TaskInfo createTask(@RequestBody final TaskForm taskForm) {

        return taskDA.createTask(taskForm);
    }

    /**
     * Получение задания студента.
     *
     * @param id
     * @return список
     */
    @Override
    @GetMapping("/getTaskByStudent/{id}")
    public TaskInfo getTaskByStudent(final @PathVariable("id") long id) {

        return taskDA.getTaskByStudent(id);
    }

    /**
     * Получение списка всех заданий.
     * @return запись
     */
    @Override
    @GetMapping("/getAllTasks")
    public List<TaskInfo> getAllTasks() {
        return taskDA.getAllTasks();
    }

    /**
     * Запись оценок студенту.
     *
     * @param assessmentsList
     * @return TaskInfo
     */
    @Override
    @PostMapping("/makeAssessment")
    public TaskInfo makeAssessment(@RequestBody final RequestToMakeAssessment assessmentsList) {

        return taskDA.makeAssessment(assessmentsList);
    }


    //----------------- Для целей отладки--------------------

    /**
     * Получаем дерево по клиенту.
     *
     * @param studentId
     * @return список
     */
    @GetMapping("/get/{student}")
    public List<Task> get(@PathVariable("studentId") final Long studentId) {

        return taskDA.getByStudent(studentId);
    }

    /**
     * Сохраняем привязку.
     *
     * @param task
     * @return объект
     */
    @PostMapping("/save")
    public Task save(@RequestBody final Task task) {
        return taskDA.save(task);
    }

}
