package ru.edu.project.backend.da.jpa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.edu.project.backend.api.tasks.Subject;
import ru.edu.project.backend.da.jpa.entity.SubjectEntity;
import ru.edu.project.backend.da.jpa.repository.SubjectEntityRepository;

import java.util.ArrayList;
import java.util.List;

@Service
public class SubjectDA {

    /**
     * Зависимость.
     */
    @Autowired
    private SubjectEntityRepository repository;

    /**
     * Получение всех заданий.
     *
     * @return список заданий
     */
    public List<Subject> getAvailable() {

        List<SubjectEntity> subjectList = repository.findAll();
        List<Subject> subjectsToReturn = map(subjectList);

        return subjectsToReturn;
    }

    private List<Subject> map(final List<SubjectEntity> subjectList) {
        List<Subject> subjectsToReturn = new ArrayList<>();

        for (SubjectEntity subjectEntity: subjectList) {

            if (subjectEntity.isEnabled()) {

                Subject subject = new Subject();

                subject.setId(subjectEntity.getId());
                subject.setTitle(subjectEntity.getTitle());
                subject.setDesc(subjectEntity.getDesc());

                subjectsToReturn.add(subject);
            }
        }
        return subjectsToReturn;
    }

}
