package ru.edu.project.backend.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.jackson.Jacksonized;

@Getter
@Setter
@Jacksonized
@Builder
public class SubjectDataModel {

    /**
     * Код предмета.
     */
    private Long id;

    /**
     * Название предмета.
     */
    private String title;

    /**
     * Описание предмета.
     */
    private String desc;

    /**
     * Активация предмета.
     */
    private boolean enabled;

}
