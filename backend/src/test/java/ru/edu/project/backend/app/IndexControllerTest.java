package ru.edu.project.backend.app;

import org.junit.Test;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;

public class IndexControllerTest {

    @Test
    public void index() {
        IndexController indexController = new IndexController();
        assertEquals("index", indexController.index());
    }
}