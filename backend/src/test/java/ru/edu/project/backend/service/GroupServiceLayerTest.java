package ru.edu.project.backend.service;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import ru.edu.project.backend.api.groups.GroupForm;
import ru.edu.project.backend.api.groups.GroupInfo;
import ru.edu.project.backend.api.students.StudentForm;
import ru.edu.project.backend.api.students.StudentInfo;
import ru.edu.project.backend.da.GroupDALayer;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;

public class GroupServiceLayerTest {

    @Mock
    private GroupDALayer daLayer;

    @InjectMocks
    private GroupServiceLayer groupServiceLayer;

    @Before
    public void setUp() throws Exception {
        openMocks(this);
    }

    @Test
    public void editGroup() {
        GroupForm groupForm = GroupForm.builder()
                .name("name")
                .description("description")
                .groupId(456L)
                .build();

        when(daLayer.save(any(GroupInfo.class))).thenAnswer(invocationOnMock -> {
            GroupInfo info = invocationOnMock.getArgument(0, GroupInfo.class);
            assertEquals(groupForm.getName(), info.getName());
            assertEquals(groupForm.getGroupId(), info.getGroupId());
            assertEquals(groupForm.getDescription(), info.getDescription());
            return info;

        });
        groupServiceLayer.editGroup(groupForm);
    }

    @Test
    public void getDetailedInfo() {
        Long groupId = 123L;
        GroupInfo groupInfo = GroupInfo.builder().build();
        when(daLayer.getById(groupId)).thenReturn(groupInfo);

        assertEquals(groupInfo, groupServiceLayer.getDetailedInfo(groupId));
        verify(daLayer).getById(groupId);
    }

    @Test
    public void getAllGroups() {
        List<GroupInfo> groupInfoList = new ArrayList<>();
        when(daLayer.getAll()).thenReturn(groupInfoList);

        assertEquals(groupInfoList, groupServiceLayer.getAllGroups());
        verify(daLayer).getAll();
    }

    @Test
    public void addAndGetStudentsCount() {
        Long groupId = 123L;
        Long expectedStudentsCount = 2L;
        GroupInfo groupInfo = GroupInfo.builder().studentsCount(1L).build();
        when(groupServiceLayer.getDetailedInfo(groupId)).thenReturn(groupInfo);

        assertEquals(expectedStudentsCount, groupServiceLayer.addAndGetStudentsCount(groupId));
        verify(daLayer).save(groupInfo);

    }
}