package ru.edu.project.backend.controller;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import ru.edu.project.backend.api.groups.GroupForm;
import ru.edu.project.backend.api.groups.GroupInfo;
import ru.edu.project.backend.service.GroupServiceLayer;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;

public class GroupControllerTest {

    @Mock
    private GroupServiceLayer delegate;

    @InjectMocks
    private GroupController groupController;

    @Before
    public void setUp() throws Exception {
        openMocks(this);
    }

    @Test
    public void editGroup() {
        GroupForm groupForm = GroupForm.builder().build();
        GroupInfo groupInfo = GroupInfo.builder().build();
        when(delegate.editGroup(groupForm)).thenReturn(groupInfo);

        assertEquals(groupInfo, groupController.editGroup(groupForm));
        verify(delegate).editGroup(groupForm);
    }

    @Test
    public void getDetailedInfo() {
        Long groupId = 123L;
        GroupInfo groupInfo = GroupInfo.builder().build();
        when(delegate.getDetailedInfo(groupId)).thenReturn(groupInfo);

        assertEquals(groupInfo, groupController.getDetailedInfo(groupId));
        verify(delegate).getDetailedInfo(groupId);
    }

    @Test
    public void getAllGroups() {
        List<GroupInfo> groupInfoList = new ArrayList<>();
        when(delegate.getAllGroups()).thenReturn(groupInfoList);

        assertEquals(groupInfoList, groupController.getAllGroups());
        verify(delegate).getAllGroups();
    }

    @Test
    public void addAndGetStudentsCount() {
        Long studentCount = 1L;
        Long groupId = 123L;
        when(delegate.addAndGetStudentsCount(groupId)).thenReturn(studentCount);

        assertEquals(studentCount, groupController.addAndGetStudentsCount(groupId));
        verify(delegate).addAndGetStudentsCount(groupId);
    }
}