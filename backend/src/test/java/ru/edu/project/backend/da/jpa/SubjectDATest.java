package ru.edu.project.backend.da.jpa;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import ru.edu.project.backend.api.tasks.Subject;
import ru.edu.project.backend.da.jpa.entity.SubjectEntity;
import ru.edu.project.backend.da.jpa.repository.SubjectEntityRepository;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;

public class SubjectDATest {

    @InjectMocks
    private SubjectDA subjectDA;

    @Mock
    private SubjectEntityRepository repository;

    @Before
    public void setUp() throws Exception {
        openMocks(this);
    }

    @Test
    public void getAvailable() {

        when(repository.findAll()).thenAnswer(invocationOnMock -> {

            List<SubjectEntity> list = new ArrayList<>();
            SubjectEntity subjectEntity = new SubjectEntity();

            long id = 123L;
            subjectEntity.setId(id);
            subjectEntity.setEnabled(true);
            subjectEntity.setTitle("subject");
            subjectEntity.setDesc("description");

            list.add(subjectEntity);

            return list;
        });

        List<Subject> expectedSubjectList = subjectDA.getAvailable();

        assertEquals(expectedSubjectList.get(0).getTitle(), "subject");
    }
}